import sys
import json
import uuid #Use this for quick and easy random alphanum gen
import requests

def generateJSON(argv):
    location = argv[0]
    approx_size = argv[1]
    json_data = []
    print(json.dumps(json_data))
    
    for i in range(0, int(approx_size)):
        json_data.append(generateTopLevel(i))
    print("Wrote file \n POSTing file")
    sendJSON(location, json_data)


def sendJSON(address, payload):
    res = requests.post(address, json=payload)
    print(res)



def generateTopLevel(index):
    data = {"index" : str(index),
            "RandInfo1": str(uuid.uuid4()),
            "RandInfo2": str(uuid.uuid4())}
    return json.dumps(data)

if __name__ == "__main__":
    for i in range(0, int(sys.argv[3])):
        generateJSON(sys.argv[1:])

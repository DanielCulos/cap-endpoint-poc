import json
import requests
import os
from flask import Flask
from flask import request
from google.cloud import pubsub_v1 as ps

app = Flask(__name__)


app.config['PROJECT'] = os.environ['GOOGLE_CLOUD_PROJECT']


def processJSON(payload):
    for i in payload:
        transformed = transformMessage(i)
        sendMessage(transformed)

def transformMessage(message):
    #Would be nice to do some reflection and
    #modular transformation loading here
    #app.logger.debug(message.upper())
    message = message.upper()
    return message
    

def sendMessage(message):
    app.logger.debug(type(message))
    publisher = ps.PublisherClient()
    topic = publisher.topic_path(
        app.config['PROJECT'],
        'send-data')
    app.logger.debug(message)
    message = str(message).encode('utf-8')
    publisher.publish(topic, message)


@app.route('/send-data', methods=['POST'])
def sendData():
    #app.logger.debug(request.data)
    tmp = json.loads(request.data.decode('utf-8'))
    app.logger.debug(type(tmp))
    #app.logger.debug(type(tmp))
    #assume is json
    processJSON(tmp)
    
    return 'Sent the Data to pubsub'

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=2300, debug=True)
